package ar.edu.unju.fi.pootp6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.pootp6.entity.Libro;
import ar.edu.unju.fi.pootp6.exception.LibroNoEncontradoException;
import ar.edu.unju.fi.pootp6.service.LibroService;

@SpringBootTest
public class LibroServiceTestCase {
	static private Libro miLibro;
	@Autowired
	LibroService service;
	
	@AfterEach
	void tearDown() {
		
	}
	
	@Test
	@DisplayName("Crear Libro")
	void crearLibro() {
		service.crear("titulo", "autor", 123, 5);
		miLibro = service.buscarPorTitulo("titulo");
		assertNotNull(miLibro);
		service.eliminar("titulo");
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("titulo"));
	}
	
	@Test
	void eliminarLibro() {
		service.crear("titulo", "autor", 123, 5);
		miLibro = service.buscarPorTitulo("titulo");
		assertNotNull(miLibro);
		service.eliminar("titulo");
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("titulo"));
	}
	
	@Test
	void editarLibro() {
		service.crear("titulo", "autor", 123, 5);
		miLibro = service.buscarPorTitulo("titulo");
		assertNotNull(miLibro);
		service.editar("titulo", "titulo nuevo", "autor nuevo", 5322, 3);
		miLibro = service.buscarPorTitulo("titulo nuevo");
		assertNotNull(miLibro);
		service.eliminar("titulo nuevo");
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("titulo"));
	}
	
	@Test
	void buscarPorTitulo() {
		service.crear("titulo", "autor", 123, 5);
		miLibro = service.buscarPorTitulo("titulo");
		assertNotNull(miLibro);
		assertEquals("titulo", miLibro.getTitulo());
		service.eliminar("titulo");
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("titulo"));
	}
	
	@Test
	void excepcionLibroNoEncontrado() {
		service.crear("titulo", "autor", 123, 5);
		miLibro = service.buscarPorTitulo("titulo");
		assertNotNull(miLibro);
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("El Quijote"));
		service.eliminar("titulo");
		assertThrows(LibroNoEncontradoException.class, () -> service.buscarPorTitulo("titulo"));
	}
}