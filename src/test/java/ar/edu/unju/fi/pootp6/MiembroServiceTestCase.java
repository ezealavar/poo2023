package ar.edu.unju.fi.pootp6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.pootp6.entity.Miembro;
import ar.edu.unju.fi.pootp6.exception.MiembroNoEncontradoException;
import ar.edu.unju.fi.pootp6.service.MiembroService;

@SpringBootTest
public class MiembroServiceTestCase {
	static private Miembro unMiembro;
	@Autowired
	MiembroService service;
	
	@AfterEach
	void tearDown() {
		
	}
	
	@Test
	void crearMiembro() {
		service.crear("Pedro", 111, "pedro@gmail.com", 388123456);
		unMiembro = service.buscarPorNumeroDeMiembro(111);
		assertNotNull(unMiembro);
		service.eliminar(111);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(111));
	}
	
	@Test
	void eliminarMiembo() {
		service.crear("Pedro", 111, "pedro@gmail.com", 388123456);
		unMiembro = service.buscarPorNumeroDeMiembro(111);
		assertNotNull(unMiembro);
		service.eliminar(111);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(111));
	}
	
	@Test
	void editarMiembro() {
		service.crear("Pedro", 111, "pedro@gmail.com", 388123456);
		unMiembro = service.buscarPorNumeroDeMiembro(111);
		assertNotNull(unMiembro);
		service.editar(111, "Ezequiel", 222, "ezequiel@gmail.com", 388367895);
		unMiembro = service.buscarPorNumeroDeMiembro(222);
		assertNotNull(unMiembro);
		service.eliminar(222);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(111));
	}
	
	@Test
	void buscarMiembroPorNumeroDeMiembro() {
		service.crear("Pedro", 111, "pedro@gmail.com", 388123456);
		unMiembro = service.buscarPorNumeroDeMiembro(111);
		assertNotNull(unMiembro);
		assertEquals(111, unMiembro.getNumero());
		service.eliminar(111);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(111));
	}
	
	@Test
	void excepcionMiembroNoEncontrado() {
		service.crear("Pedro", 111, "pedro@gmail.com", 388123456);
		unMiembro = service.buscarPorNumeroDeMiembro(111);
		assertNotNull(unMiembro);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(222));
		service.eliminar(111);
		assertThrows(MiembroNoEncontradoException.class, () -> service.buscarPorNumeroDeMiembro(111));
	}
}
