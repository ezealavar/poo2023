package ar.edu.unju.fi.pootp6.service;

import ar.edu.unju.fi.pootp6.entity.Libro;

public interface LibroService {
	public void crear(String titulo, String autor, int isbn, int cantidadDisponible);
	public void eliminar(String titulo);
	public void editar(String tituloBuscado, String tituloNuevo, String autorNuevo, int isbnNuevo, int cantidadDisponibleNuevo);
	public Libro buscarPorTitulo(String titulo);
	public Libro buscarPorId(Long id);
}
