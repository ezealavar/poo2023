package ar.edu.unju.fi.pootp6.exception;

public class LibroNoEncontradoException extends RuntimeException{
	private static final long serialVersionUID = 0;
	
	public LibroNoEncontradoException(String mensaje) {
		super(mensaje);
	}
}
