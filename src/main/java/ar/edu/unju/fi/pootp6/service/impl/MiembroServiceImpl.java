package ar.edu.unju.fi.pootp6.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.pootp6.entity.Miembro;
import ar.edu.unju.fi.pootp6.exception.MiembroNoEncontradoException;
import ar.edu.unju.fi.pootp6.repository.MiembroRepository;
import ar.edu.unju.fi.pootp6.service.MiembroService;

@Service
public class MiembroServiceImpl implements MiembroService{
	
	@Autowired
	MiembroRepository miembroRepository; 
	
	public void crear(String nombre, int numero, String correo, int telefono) {
		Miembro miembroNuevo = new Miembro(nombre ,numero, correo, telefono);
		miembroRepository.save(miembroNuevo);
	}
	
	public void eliminar(int numero) {
		Miembro miembroBuscado = buscarPorNumeroDeMiembro(numero);
		miembroRepository.delete(miembroBuscado);
	}
	
	public void editar(int numeroBuscado, String nombreNuevo, int numeroNuevo, String correoNuevo, int telefonoNuevo) {
		Miembro miembroBuscado = buscarPorNumeroDeMiembro(numeroBuscado);
		miembroBuscado.setNombre(nombreNuevo);
		miembroBuscado.setNumero(numeroNuevo);
		miembroBuscado.setCorreo(correoNuevo);
		miembroBuscado.setTelefono(telefonoNuevo);
		miembroRepository.save(miembroBuscado);
	}
	
	public Miembro buscarPorNumeroDeMiembro(int numero) {
		Iterable<Miembro>miembros = miembroRepository.findAll();
		Miembro miembroBuscado = null;
		
		for(Miembro m: miembros) {
			if(m.getNumero() == numero) {
				miembroBuscado = m;
				return miembroBuscado;
			}
		}
		throw new MiembroNoEncontradoException("No se encontro al miembro con el numero: " + numero);
	}
}
