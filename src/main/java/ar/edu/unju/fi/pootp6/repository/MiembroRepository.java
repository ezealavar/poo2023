package ar.edu.unju.fi.pootp6.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.pootp6.entity.Miembro;

@Repository
public interface MiembroRepository extends CrudRepository<Miembro, Long>{
	
}
