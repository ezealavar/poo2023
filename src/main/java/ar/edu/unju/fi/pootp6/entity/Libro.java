package ar.edu.unju.fi.pootp6.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Libro {
	
	public Libro() {
	
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	
	@Column(name = "titulo")
	 private String titulo;
	
	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", autor=" + autor + ", isbn=" + isbn
				+ ", cantidadDisponible=" + cantidadDisponible + "]";
	}

	@Column(name = "autor")
	 private String autor;
	
	@Column(name = "isbn")
	 private int isbn;

	@Column(name = "cantidad disponible")
	 private int cantidadDisponible;
	 
	public Libro(String titulo, String autor, int isbn, int cantidadDisponible) {
		this.titulo = titulo;
		this.autor = autor;
		this.isbn = isbn;
		this.cantidadDisponible = cantidadDisponible;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public int getCantidadDisponible() {
		return cantidadDisponible;
	}

	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}
}
