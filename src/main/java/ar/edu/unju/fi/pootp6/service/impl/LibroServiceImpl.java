package ar.edu.unju.fi.pootp6.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.pootp6.entity.Libro;
import ar.edu.unju.fi.pootp6.exception.LibroNoEncontradoException;
import ar.edu.unju.fi.pootp6.repository.LibroRepository;
import ar.edu.unju.fi.pootp6.service.LibroService;

@Service
public class LibroServiceImpl implements LibroService{
	
	@Autowired
	LibroRepository libroRepository;
	
	public Libro buscarPorId(Long id) {
		return libroRepository.findById(id).isPresent() ? libroRepository.findById(id).get() : null;
	}
	
	public void crear(String titulo, String autor, int isbn, int cantidadDisponible) {
		Libro miLibro = new Libro(titulo, autor, isbn, cantidadDisponible);
		libroRepository.save(miLibro);
	}
	
	public void eliminar(String titulo) {
		Libro miLibro = buscarPorTitulo(titulo);
		libroRepository.delete(miLibro);
	}
	
	public void editar(String tituloBuscado, String tituloNuevo, String autorNuevo, int isbnNuevo, int cantidadDisponibleNuevo) {
		
		Libro miLibro = buscarPorTitulo(tituloBuscado);
		miLibro.setTitulo(tituloNuevo);
		miLibro.setAutor(autorNuevo);
		miLibro.setIsbn(isbnNuevo);
		miLibro.setCantidadDisponible(cantidadDisponibleNuevo);
		libroRepository.save(miLibro);
	}
	
	public Libro buscarPorTitulo(String titulo) {
		Iterable<Libro>libros = libroRepository.findAll();
		Libro libro = null;
		
		for(Libro l: libros)
			if(l.getTitulo().equals(titulo)) {
				libro = l;
				return libro;
			}
		throw new LibroNoEncontradoException("No se encontro el libro: " + titulo);
	}
}
