package ar.edu.unju.fi.pootp6.exception;

public class MiembroNoEncontradoException extends RuntimeException{
	private static final long serialVersionUID = 0;
	
	public MiembroNoEncontradoException(String mensaje) {
		super(mensaje);
	}
}
