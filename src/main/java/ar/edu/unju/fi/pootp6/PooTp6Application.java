package ar.edu.unju.fi.pootp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PooTp6Application {

	public static void main(String[] args) {
		SpringApplication.run(PooTp6Application.class, args);
	}

}
