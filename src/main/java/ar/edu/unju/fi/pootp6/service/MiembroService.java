package ar.edu.unju.fi.pootp6.service;

import ar.edu.unju.fi.pootp6.entity.Miembro;

public interface MiembroService {
	public void crear(String nombre, int numero, String correo, int telefono);
	public void eliminar(int numero);
	public void editar(int numeroBuscado, String nombreNuevo, int numeroNuevo, String correoNuevo, int telefonoNuevo);
	public Miembro buscarPorNumeroDeMiembro(int numero);
}
